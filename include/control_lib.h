#pragma once
#include "eigen3/Eigen/Dense"
#include "kinematic_model_lib.h"

// Simple proportional controller of a planar 2DoF serial manipulator
class Controller{
  public:
    Controller();
    // Feedback desired joint velocity taking 
          //  actual cartesian position `x`, 
          //  desired cartesian position `xd`, 
          //  cartesian feedforward velocity `Dxd_ff` 
          //  and jacobian matrix `J`
    Eigen::Vector2d   Dqd ( 
              const Eigen::Vector2d & x,
              const Eigen::Vector2d & xd,
              const Eigen::Vector2d & Dxd_ff,
              const Eigen::Matrix2d & J
                          );
  private:
    const double      kp          { 1e2                     };
    Eigen::Matrix2d   J           { Eigen::Matrix2d::Zero() };
    Eigen::Vector2d   dX_desired  { Eigen::Vector2d::Zero() };    
    Eigen::Vector2d   X           { Eigen::Vector2d::Zero() };    
};