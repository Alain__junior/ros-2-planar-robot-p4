#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "eigen3/Eigen/Dense"
#include "rclcpp/rclcpp.hpp"
#include "ros2_planar_robot/msg/joint_position.hpp"


using namespace std::chrono_literals;
using std::placeholders::_1;

// Node publishing joint states that are used to visualize the robot in rviz
class JointPub : public rclcpp::Node
{
  public:
    JointPub()
    : Node("JointPub")
    {
      pub_pos_ = this->create_publisher    <sensor_msgs::msg::JointState>(
            "joint_states",10
        );
       subs_joint_position_ = this->create_subscription <ros2_planar_robot::msg::JointPosition>   (
            "q", 10, std::bind(&JointPub::topic_callback, this, _1)
        );
    }
  private:
  void topic_callback(const ros2_planar_robot::msg::JointPosition::SharedPtr joint_position)
  {
    q_ << joint_position->q[0], joint_position->q[1];
    timer_callback();
  }
  void timer_callback()
  {
    auto message  = sensor_msgs::msg::JointState();
    rclcpp::Time now = this->get_clock()->now();
    message.header.stamp = now;
    message.name = {"r1","r2"}; // Check joint names in the urdf file
    message.position = {q_(0),q_(1)};
    pub_pos_ ->publish(message);

  }
 Eigen::Vector2d     q_        { Eigen::Vector2d::Zero() } ;
rclcpp::Publisher<sensor_msgs::msg::JointState>          ::SharedPtr   pub_pos_ ;   
rclcpp::Subscription<ros2_planar_robot::msg::JointPosition> ::SharedPtr  subs_joint_position_   ; 
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<JointPub>());
  rclcpp::shutdown();
  return 0;
}