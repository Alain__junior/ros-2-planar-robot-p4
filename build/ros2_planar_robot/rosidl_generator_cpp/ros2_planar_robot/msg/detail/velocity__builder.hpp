// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from ros2_planar_robot:msg/Velocity.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__VELOCITY__BUILDER_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__VELOCITY__BUILDER_HPP_

#include <algorithm>
#include <utility>

#include "ros2_planar_robot/msg/detail/velocity__struct.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


namespace ros2_planar_robot
{

namespace msg
{

namespace builder
{

class Init_Velocity_dy
{
public:
  explicit Init_Velocity_dy(::ros2_planar_robot::msg::Velocity & msg)
  : msg_(msg)
  {}
  ::ros2_planar_robot::msg::Velocity dy(::ros2_planar_robot::msg::Velocity::_dy_type arg)
  {
    msg_.dy = std::move(arg);
    return std::move(msg_);
  }

private:
  ::ros2_planar_robot::msg::Velocity msg_;
};

class Init_Velocity_dx
{
public:
  Init_Velocity_dx()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_Velocity_dy dx(::ros2_planar_robot::msg::Velocity::_dx_type arg)
  {
    msg_.dx = std::move(arg);
    return Init_Velocity_dy(msg_);
  }

private:
  ::ros2_planar_robot::msg::Velocity msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::ros2_planar_robot::msg::Velocity>()
{
  return ros2_planar_robot::msg::builder::Init_Velocity_dx();
}

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__VELOCITY__BUILDER_HPP_
