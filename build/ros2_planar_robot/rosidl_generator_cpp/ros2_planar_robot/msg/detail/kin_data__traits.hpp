// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ros2_planar_robot:msg/KinData.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__TRAITS_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "ros2_planar_robot/msg/detail/kin_data__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

// Include directives for member types
// Member 'pose'
#include "ros2_planar_robot/msg/detail/pose__traits.hpp"

namespace ros2_planar_robot
{

namespace msg
{

inline void to_flow_style_yaml(
  const KinData & msg,
  std::ostream & out)
{
  out << "{";
  // member: pose
  {
    out << "pose: ";
    to_flow_style_yaml(msg.pose, out);
    out << ", ";
  }

  // member: jacobian
  {
    if (msg.jacobian.size() == 0) {
      out << "jacobian: []";
    } else {
      out << "jacobian: [";
      size_t pending_items = msg.jacobian.size();
      for (auto item : msg.jacobian) {
        rosidl_generator_traits::value_to_yaml(item, out);
        if (--pending_items > 0) {
          out << ", ";
        }
      }
      out << "]";
    }
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const KinData & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: pose
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "pose:\n";
    to_block_style_yaml(msg.pose, out, indentation + 2);
  }

  // member: jacobian
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    if (msg.jacobian.size() == 0) {
      out << "jacobian: []\n";
    } else {
      out << "jacobian:\n";
      for (auto item : msg.jacobian) {
        if (indentation > 0) {
          out << std::string(indentation, ' ');
        }
        out << "- ";
        rosidl_generator_traits::value_to_yaml(item, out);
        out << "\n";
      }
    }
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const KinData & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace msg

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::msg::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::msg::KinData & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::msg::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::msg::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::msg::KinData & msg)
{
  return ros2_planar_robot::msg::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::msg::KinData>()
{
  return "ros2_planar_robot::msg::KinData";
}

template<>
inline const char * name<ros2_planar_robot::msg::KinData>()
{
  return "ros2_planar_robot/msg/KinData";
}

template<>
struct has_fixed_size<ros2_planar_robot::msg::KinData>
  : std::integral_constant<bool, has_fixed_size<ros2_planar_robot::msg::Pose>::value> {};

template<>
struct has_bounded_size<ros2_planar_robot::msg::KinData>
  : std::integral_constant<bool, has_bounded_size<ros2_planar_robot::msg::Pose>::value> {};

template<>
struct is_message<ros2_planar_robot::msg::KinData>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__KIN_DATA__TRAITS_HPP_
