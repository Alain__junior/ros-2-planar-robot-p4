// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from ros2_planar_robot:msg/CartesianState.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__STRUCT_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__STRUCT_HPP_

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

#include "rosidl_runtime_cpp/bounded_vector.hpp"
#include "rosidl_runtime_cpp/message_initialization.hpp"


// Include directives for member types
// Member 'pose'
#include "ros2_planar_robot/msg/detail/pose__struct.hpp"
// Member 'velocity'
#include "ros2_planar_robot/msg/detail/velocity__struct.hpp"

#ifndef _WIN32
# define DEPRECATED__ros2_planar_robot__msg__CartesianState __attribute__((deprecated))
#else
# define DEPRECATED__ros2_planar_robot__msg__CartesianState __declspec(deprecated)
#endif

namespace ros2_planar_robot
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct CartesianState_
{
  using Type = CartesianState_<ContainerAllocator>;

  explicit CartesianState_(rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : pose(_init),
    velocity(_init)
  {
    (void)_init;
  }

  explicit CartesianState_(const ContainerAllocator & _alloc, rosidl_runtime_cpp::MessageInitialization _init = rosidl_runtime_cpp::MessageInitialization::ALL)
  : pose(_alloc, _init),
    velocity(_alloc, _init)
  {
    (void)_init;
  }

  // field types and members
  using _pose_type =
    ros2_planar_robot::msg::Pose_<ContainerAllocator>;
  _pose_type pose;
  using _velocity_type =
    ros2_planar_robot::msg::Velocity_<ContainerAllocator>;
  _velocity_type velocity;

  // setters for named parameter idiom
  Type & set__pose(
    const ros2_planar_robot::msg::Pose_<ContainerAllocator> & _arg)
  {
    this->pose = _arg;
    return *this;
  }
  Type & set__velocity(
    const ros2_planar_robot::msg::Velocity_<ContainerAllocator> & _arg)
  {
    this->velocity = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    ros2_planar_robot::msg::CartesianState_<ContainerAllocator> *;
  using ConstRawPtr =
    const ros2_planar_robot::msg::CartesianState_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::CartesianState_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      ros2_planar_robot::msg::CartesianState_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__ros2_planar_robot__msg__CartesianState
    std::shared_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__ros2_planar_robot__msg__CartesianState
    std::shared_ptr<ros2_planar_robot::msg::CartesianState_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const CartesianState_ & other) const
  {
    if (this->pose != other.pose) {
      return false;
    }
    if (this->velocity != other.velocity) {
      return false;
    }
    return true;
  }
  bool operator!=(const CartesianState_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct CartesianState_

// alias to use template instance with default allocator
using CartesianState =
  ros2_planar_robot::msg::CartesianState_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace ros2_planar_robot

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__STRUCT_HPP_
