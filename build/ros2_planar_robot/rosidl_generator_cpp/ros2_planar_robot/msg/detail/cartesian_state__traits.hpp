// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ros2_planar_robot:msg/CartesianState.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__TRAITS_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "ros2_planar_robot/msg/detail/cartesian_state__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

// Include directives for member types
// Member 'pose'
#include "ros2_planar_robot/msg/detail/pose__traits.hpp"
// Member 'velocity'
#include "ros2_planar_robot/msg/detail/velocity__traits.hpp"

namespace ros2_planar_robot
{

namespace msg
{

inline void to_flow_style_yaml(
  const CartesianState & msg,
  std::ostream & out)
{
  out << "{";
  // member: pose
  {
    out << "pose: ";
    to_flow_style_yaml(msg.pose, out);
    out << ", ";
  }

  // member: velocity
  {
    out << "velocity: ";
    to_flow_style_yaml(msg.velocity, out);
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const CartesianState & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: pose
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "pose:\n";
    to_block_style_yaml(msg.pose, out, indentation + 2);
  }

  // member: velocity
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "velocity:\n";
    to_block_style_yaml(msg.velocity, out, indentation + 2);
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const CartesianState & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace msg

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::msg::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::msg::CartesianState & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::msg::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::msg::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::msg::CartesianState & msg)
{
  return ros2_planar_robot::msg::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::msg::CartesianState>()
{
  return "ros2_planar_robot::msg::CartesianState";
}

template<>
inline const char * name<ros2_planar_robot::msg::CartesianState>()
{
  return "ros2_planar_robot/msg/CartesianState";
}

template<>
struct has_fixed_size<ros2_planar_robot::msg::CartesianState>
  : std::integral_constant<bool, has_fixed_size<ros2_planar_robot::msg::Pose>::value && has_fixed_size<ros2_planar_robot::msg::Velocity>::value> {};

template<>
struct has_bounded_size<ros2_planar_robot::msg::CartesianState>
  : std::integral_constant<bool, has_bounded_size<ros2_planar_robot::msg::Pose>::value && has_bounded_size<ros2_planar_robot::msg::Velocity>::value> {};

template<>
struct is_message<ros2_planar_robot::msg::CartesianState>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__CARTESIAN_STATE__TRAITS_HPP_
