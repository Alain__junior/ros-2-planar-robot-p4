from setuptools import find_packages
from setuptools import setup

setup(
    name='ros2_planar_robot',
    version='0.0.0',
    packages=find_packages(
        include=('ros2_planar_robot', 'ros2_planar_robot.*')),
)
