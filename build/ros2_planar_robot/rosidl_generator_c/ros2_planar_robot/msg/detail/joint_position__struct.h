// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from ros2_planar_robot:msg/JointPosition.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_H_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

/// Struct defined in msg/JointPosition in the package ros2_planar_robot.
typedef struct ros2_planar_robot__msg__JointPosition
{
  double q[2];
} ros2_planar_robot__msg__JointPosition;

// Struct for a sequence of ros2_planar_robot__msg__JointPosition.
typedef struct ros2_planar_robot__msg__JointPosition__Sequence
{
  ros2_planar_robot__msg__JointPosition * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} ros2_planar_robot__msg__JointPosition__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__STRUCT_H_
