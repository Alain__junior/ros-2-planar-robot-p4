// generated from rosidl_generator_c/resource/idl.h.em
// with input from ros2_planar_robot:msg/Pose.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__POSE_H_
#define ROS2_PLANAR_ROBOT__MSG__POSE_H_

#include "ros2_planar_robot/msg/detail/pose__struct.h"
#include "ros2_planar_robot/msg/detail/pose__functions.h"
#include "ros2_planar_robot/msg/detail/pose__type_support.h"

#endif  // ROS2_PLANAR_ROBOT__MSG__POSE_H_
