// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ros2_planar_robot:msg/JointPosition.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__TRAITS_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "ros2_planar_robot/msg/detail/joint_position__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

namespace ros2_planar_robot
{

namespace msg
{

inline void to_flow_style_yaml(
  const JointPosition & msg,
  std::ostream & out)
{
  out << "{";
  // member: q
  {
    if (msg.q.size() == 0) {
      out << "q: []";
    } else {
      out << "q: [";
      size_t pending_items = msg.q.size();
      for (auto item : msg.q) {
        rosidl_generator_traits::value_to_yaml(item, out);
        if (--pending_items > 0) {
          out << ", ";
        }
      }
      out << "]";
    }
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const JointPosition & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: q
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    if (msg.q.size() == 0) {
      out << "q: []\n";
    } else {
      out << "q:\n";
      for (auto item : msg.q) {
        if (indentation > 0) {
          out << std::string(indentation, ' ');
        }
        out << "- ";
        rosidl_generator_traits::value_to_yaml(item, out);
        out << "\n";
      }
    }
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const JointPosition & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace msg

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::msg::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::msg::JointPosition & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::msg::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::msg::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::msg::JointPosition & msg)
{
  return ros2_planar_robot::msg::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::msg::JointPosition>()
{
  return "ros2_planar_robot::msg::JointPosition";
}

template<>
inline const char * name<ros2_planar_robot::msg::JointPosition>()
{
  return "ros2_planar_robot/msg/JointPosition";
}

template<>
struct has_fixed_size<ros2_planar_robot::msg::JointPosition>
  : std::integral_constant<bool, true> {};

template<>
struct has_bounded_size<ros2_planar_robot::msg::JointPosition>
  : std::integral_constant<bool, true> {};

template<>
struct is_message<ros2_planar_robot::msg::JointPosition>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__JOINT_POSITION__TRAITS_HPP_
