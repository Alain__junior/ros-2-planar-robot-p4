// generated from rosidl_generator_c/resource/idl__functions.c.em
// with input from ros2_planar_robot:msg/JointVelocity.idl
// generated code does not contain a copyright notice
#include "ros2_planar_robot/msg/detail/joint_velocity__functions.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "rcutils/allocator.h"


bool
ros2_planar_robot__msg__JointVelocity__init(ros2_planar_robot__msg__JointVelocity * msg)
{
  if (!msg) {
    return false;
  }
  // dq
  return true;
}

void
ros2_planar_robot__msg__JointVelocity__fini(ros2_planar_robot__msg__JointVelocity * msg)
{
  if (!msg) {
    return;
  }
  // dq
}

bool
ros2_planar_robot__msg__JointVelocity__are_equal(const ros2_planar_robot__msg__JointVelocity * lhs, const ros2_planar_robot__msg__JointVelocity * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  // dq
  for (size_t i = 0; i < 2; ++i) {
    if (lhs->dq[i] != rhs->dq[i]) {
      return false;
    }
  }
  return true;
}

bool
ros2_planar_robot__msg__JointVelocity__copy(
  const ros2_planar_robot__msg__JointVelocity * input,
  ros2_planar_robot__msg__JointVelocity * output)
{
  if (!input || !output) {
    return false;
  }
  // dq
  for (size_t i = 0; i < 2; ++i) {
    output->dq[i] = input->dq[i];
  }
  return true;
}

ros2_planar_robot__msg__JointVelocity *
ros2_planar_robot__msg__JointVelocity__create()
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__JointVelocity * msg = (ros2_planar_robot__msg__JointVelocity *)allocator.allocate(sizeof(ros2_planar_robot__msg__JointVelocity), allocator.state);
  if (!msg) {
    return NULL;
  }
  memset(msg, 0, sizeof(ros2_planar_robot__msg__JointVelocity));
  bool success = ros2_planar_robot__msg__JointVelocity__init(msg);
  if (!success) {
    allocator.deallocate(msg, allocator.state);
    return NULL;
  }
  return msg;
}

void
ros2_planar_robot__msg__JointVelocity__destroy(ros2_planar_robot__msg__JointVelocity * msg)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (msg) {
    ros2_planar_robot__msg__JointVelocity__fini(msg);
  }
  allocator.deallocate(msg, allocator.state);
}


bool
ros2_planar_robot__msg__JointVelocity__Sequence__init(ros2_planar_robot__msg__JointVelocity__Sequence * array, size_t size)
{
  if (!array) {
    return false;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__JointVelocity * data = NULL;

  if (size) {
    data = (ros2_planar_robot__msg__JointVelocity *)allocator.zero_allocate(size, sizeof(ros2_planar_robot__msg__JointVelocity), allocator.state);
    if (!data) {
      return false;
    }
    // initialize all array elements
    size_t i;
    for (i = 0; i < size; ++i) {
      bool success = ros2_planar_robot__msg__JointVelocity__init(&data[i]);
      if (!success) {
        break;
      }
    }
    if (i < size) {
      // if initialization failed finalize the already initialized array elements
      for (; i > 0; --i) {
        ros2_planar_robot__msg__JointVelocity__fini(&data[i - 1]);
      }
      allocator.deallocate(data, allocator.state);
      return false;
    }
  }
  array->data = data;
  array->size = size;
  array->capacity = size;
  return true;
}

void
ros2_planar_robot__msg__JointVelocity__Sequence__fini(ros2_planar_robot__msg__JointVelocity__Sequence * array)
{
  if (!array) {
    return;
  }
  rcutils_allocator_t allocator = rcutils_get_default_allocator();

  if (array->data) {
    // ensure that data and capacity values are consistent
    assert(array->capacity > 0);
    // finalize all array elements
    for (size_t i = 0; i < array->capacity; ++i) {
      ros2_planar_robot__msg__JointVelocity__fini(&array->data[i]);
    }
    allocator.deallocate(array->data, allocator.state);
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
  } else {
    // ensure that data, size, and capacity values are consistent
    assert(0 == array->size);
    assert(0 == array->capacity);
  }
}

ros2_planar_robot__msg__JointVelocity__Sequence *
ros2_planar_robot__msg__JointVelocity__Sequence__create(size_t size)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  ros2_planar_robot__msg__JointVelocity__Sequence * array = (ros2_planar_robot__msg__JointVelocity__Sequence *)allocator.allocate(sizeof(ros2_planar_robot__msg__JointVelocity__Sequence), allocator.state);
  if (!array) {
    return NULL;
  }
  bool success = ros2_planar_robot__msg__JointVelocity__Sequence__init(array, size);
  if (!success) {
    allocator.deallocate(array, allocator.state);
    return NULL;
  }
  return array;
}

void
ros2_planar_robot__msg__JointVelocity__Sequence__destroy(ros2_planar_robot__msg__JointVelocity__Sequence * array)
{
  rcutils_allocator_t allocator = rcutils_get_default_allocator();
  if (array) {
    ros2_planar_robot__msg__JointVelocity__Sequence__fini(array);
  }
  allocator.deallocate(array, allocator.state);
}

bool
ros2_planar_robot__msg__JointVelocity__Sequence__are_equal(const ros2_planar_robot__msg__JointVelocity__Sequence * lhs, const ros2_planar_robot__msg__JointVelocity__Sequence * rhs)
{
  if (!lhs || !rhs) {
    return false;
  }
  if (lhs->size != rhs->size) {
    return false;
  }
  for (size_t i = 0; i < lhs->size; ++i) {
    if (!ros2_planar_robot__msg__JointVelocity__are_equal(&(lhs->data[i]), &(rhs->data[i]))) {
      return false;
    }
  }
  return true;
}

bool
ros2_planar_robot__msg__JointVelocity__Sequence__copy(
  const ros2_planar_robot__msg__JointVelocity__Sequence * input,
  ros2_planar_robot__msg__JointVelocity__Sequence * output)
{
  if (!input || !output) {
    return false;
  }
  if (output->capacity < input->size) {
    const size_t allocation_size =
      input->size * sizeof(ros2_planar_robot__msg__JointVelocity);
    rcutils_allocator_t allocator = rcutils_get_default_allocator();
    ros2_planar_robot__msg__JointVelocity * data =
      (ros2_planar_robot__msg__JointVelocity *)allocator.reallocate(
      output->data, allocation_size, allocator.state);
    if (!data) {
      return false;
    }
    // If reallocation succeeded, memory may or may not have been moved
    // to fulfill the allocation request, invalidating output->data.
    output->data = data;
    for (size_t i = output->capacity; i < input->size; ++i) {
      if (!ros2_planar_robot__msg__JointVelocity__init(&output->data[i])) {
        // If initialization of any new item fails, roll back
        // all previously initialized items. Existing items
        // in output are to be left unmodified.
        for (; i-- > output->capacity; ) {
          ros2_planar_robot__msg__JointVelocity__fini(&output->data[i]);
        }
        return false;
      }
    }
    output->capacity = input->size;
  }
  output->size = input->size;
  for (size_t i = 0; i < input->size; ++i) {
    if (!ros2_planar_robot__msg__JointVelocity__copy(
        &(input->data[i]), &(output->data[i])))
    {
      return false;
    }
  }
  return true;
}
