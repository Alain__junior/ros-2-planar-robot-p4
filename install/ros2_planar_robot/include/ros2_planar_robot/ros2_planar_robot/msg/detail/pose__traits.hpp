// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from ros2_planar_robot:msg/Pose.idl
// generated code does not contain a copyright notice

#ifndef ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__TRAITS_HPP_
#define ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__TRAITS_HPP_

#include <stdint.h>

#include <sstream>
#include <string>
#include <type_traits>

#include "ros2_planar_robot/msg/detail/pose__struct.hpp"
#include "rosidl_runtime_cpp/traits.hpp"

namespace ros2_planar_robot
{

namespace msg
{

inline void to_flow_style_yaml(
  const Pose & msg,
  std::ostream & out)
{
  out << "{";
  // member: x
  {
    out << "x: ";
    rosidl_generator_traits::value_to_yaml(msg.x, out);
    out << ", ";
  }

  // member: y
  {
    out << "y: ";
    rosidl_generator_traits::value_to_yaml(msg.y, out);
  }
  out << "}";
}  // NOLINT(readability/fn_size)

inline void to_block_style_yaml(
  const Pose & msg,
  std::ostream & out, size_t indentation = 0)
{
  // member: x
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "x: ";
    rosidl_generator_traits::value_to_yaml(msg.x, out);
    out << "\n";
  }

  // member: y
  {
    if (indentation > 0) {
      out << std::string(indentation, ' ');
    }
    out << "y: ";
    rosidl_generator_traits::value_to_yaml(msg.y, out);
    out << "\n";
  }
}  // NOLINT(readability/fn_size)

inline std::string to_yaml(const Pose & msg, bool use_flow_style = false)
{
  std::ostringstream out;
  if (use_flow_style) {
    to_flow_style_yaml(msg, out);
  } else {
    to_block_style_yaml(msg, out);
  }
  return out.str();
}

}  // namespace msg

}  // namespace ros2_planar_robot

namespace rosidl_generator_traits
{

[[deprecated("use ros2_planar_robot::msg::to_block_style_yaml() instead")]]
inline void to_yaml(
  const ros2_planar_robot::msg::Pose & msg,
  std::ostream & out, size_t indentation = 0)
{
  ros2_planar_robot::msg::to_block_style_yaml(msg, out, indentation);
}

[[deprecated("use ros2_planar_robot::msg::to_yaml() instead")]]
inline std::string to_yaml(const ros2_planar_robot::msg::Pose & msg)
{
  return ros2_planar_robot::msg::to_yaml(msg);
}

template<>
inline const char * data_type<ros2_planar_robot::msg::Pose>()
{
  return "ros2_planar_robot::msg::Pose";
}

template<>
inline const char * name<ros2_planar_robot::msg::Pose>()
{
  return "ros2_planar_robot/msg/Pose";
}

template<>
struct has_fixed_size<ros2_planar_robot::msg::Pose>
  : std::integral_constant<bool, true> {};

template<>
struct has_bounded_size<ros2_planar_robot::msg::Pose>
  : std::integral_constant<bool, true> {};

template<>
struct is_message<ros2_planar_robot::msg::Pose>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // ROS2_PLANAR_ROBOT__MSG__DETAIL__POSE__TRAITS_HPP_
